from src.word_analitics import filter_words_containing_letter_a


def test_filtering_words_for_empty_list():
    filtered_list = filter_words_containing_letter_a([])
    assert filtered_list == []


def test_filtered_words_for_list_of_integer():
    filtered_list = filter_words_containing_letter_a([1, 2, 3])
    assert filtered_list == []


def test_filtered_words_for_list_of_special_charaters():
    filtered_list = filter_words_containing_letter_a(['#', '@'])
    assert filtered_list == []


def test_filtered_words_for_list_without_a():
    filtered_list = filter_words_containing_letter_a(['los', 'nos', 'cios'])
    assert filtered_list == []


def test_filtered_words_for_list_with_a():
    list = ['maka', 'paka', 'draka']
    filtered_list = filter_words_containing_letter_a(list)
    assert filtered_list == list


def test_filtered_different_word():
    list = ['ola', 'mirc', 'jan', 'szymon', 'piotr']
    filtered_list = filter_words_containing_letter_a(list)
    assert filtered_list == ['ola', 'jan']

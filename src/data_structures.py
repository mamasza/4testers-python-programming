favourite_movies = ['the Mission', 'Gone with the Wind', 'Sleepers', 'Lord of the Rings', 'Nikita', 'Blue']
favourite_movies.append('Red')
favourite_movies.append('White')
print(len(favourite_movies))
middle_movies = favourite_movies[2:6]
print(middle_movies)
favourite_movies.insert(0, 'Roma')
print(favourite_movies)
middle_movies = favourite_movies[2:6]
print(middle_movies)

email = ['a@example.com', 'b@example.com']
print(f"Number of elements is {len(email)}, where 1st is {email[0]} and 2nd is {email[-1]}")
friend = {
    'first_name': 'Anna',
    'age': 45,
    'hobby' : ['skiing', 'climbing']
}

friend_hobbies = friend['hobby']
print(f"hobbies of my friend: {friend_hobbies}")
print (f"My friend has {len(friend_hobbies)} hobbies")
friend['hobby'].append('football')
print(friend)
friend['married'] = True
friend['age']=44
print(friend)
list_temp =[1,2,3,4]
def calculate_lenght_of_the_list(list=[]):
    return len(list)

def calculate_sum_of_the_list(list=[]):
    return sum

print(calculate_lenght_of_the_list(list_temp))
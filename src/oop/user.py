class User:
    def __init__(self, email, gender):
        self.email = email
        self.gender = gender
        self.usage_time = 0

    def use_application(self, usage_time_in_seconds):
        self.usage_time += usage_time_in_seconds


if __name__ == '__main__':
    user_kate = User('kate@example.com', 'female')
    user_james = User('james@exmaple.com', 'male')

    print(user_kate.email)
    print(user_james.email)

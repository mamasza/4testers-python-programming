def print_greetings_for_a_person_in_the_city(name, city):
    print(f"Witaj {name}! Miło cię widzieć w naszym mieście - {city}")


print_greetings_for_a_person_in_the_city("anna", "szczecin")


def create_new_email_adress(first_name, last_name):
    return f"{first_name.lower()}.{last_name.lower()}@tester.pl"


if __name__ == '__main__':
    print(create_new_email_adress("Janusz", "Nowak"))
    print(create_new_email_adress("Barbara", "Kowalska"))

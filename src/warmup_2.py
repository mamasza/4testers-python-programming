def square_of_the_input_number(input_number):
    return input_number ** 2


def calculate_celcjus_to_farenheit(celsjus):
    return celsjus * 9 / 2 + 32


def calculate_volume_of_the_cuboid(a, b, c):
    return a * b * c


if __name__ == '__main__':
    # zad 1
    print(square_of_the_input_number(0))
    print(square_of_the_input_number(16))
    print(square_of_the_input_number(2.55))

    # zad2
    print(f"Temperature given by you is {calculate_celcjus_to_farenheit(2)} Farenheit's degree")

    # zad3
    print(calculate_volume_of_the_cuboid(3, 5, 7))
